import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }

    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer mini = null;
        for(Integer i : liste){
          if(mini == null){
            mini = i;
          }
          else{
            if(i.compareTo(mini) < 0){
              mini = i;
            }
          }
        }
        return mini;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        boolean res = true;
        for(int i = 0; i<liste.size(); i++){
          if(liste.get(i).compareTo(valeur)<=0){
            res = false;
          }
        }

        return res;
    }

    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> res = new ArrayList<>();
        boolean sortir = false;
        int j = 0;
        for(int i = 0; i<liste1.size(); i++){
          sortir = false;
          j=0;
          while(j < liste2.size() && !sortir){
            if(liste1.get(i).compareTo(liste2.get(j)) == 0){
              if(!res.contains(liste1.get(i))){
                res.add(liste1.get(i));
              }
              sortir = true;
            }
            if(liste2.get(j).compareTo(liste1.get(i)) > 0){
              sortir = true;
            }
            j++;
          }
        }
      return res;
      }

    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> res = new ArrayList<>();
        if(texte.length() == 0){
          return res;
        }
        String motCourant = "";
        for(int i = 0; i < texte.length(); i++){
          Character cara = texte.charAt(i);
          String car = cara.toString();
          if(car.compareTo(" ") == 0){
            if(motCourant.length() > 0){
              res.add(motCourant);
              motCourant = "";
            }
          }
          else{
            motCourant = motCourant + car;
          }
        }
        if(motCourant != ""){
          res.add(motCourant);
        }
      return res;
    }

    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        String motCourant = "";
        Map<String,Integer> dicomots = new HashMap<>();
        for(int i =0; i<texte.length(); i++){
            if(texte.charAt(i) == ' '){
              if(motCourant != ""){
                if(dicomots.containsKey(motCourant)){
                  dicomots.put(motCourant,dicomots.get(motCourant)+1);
                }
                else{
                  dicomots.put(motCourant,1);
                }
              }
              motCourant = "";
            }
            else{
              char car = texte.charAt(i);
              motCourant = motCourant + car;
            }
        }
        if(dicomots.containsKey(motCourant)){
          dicomots.put(motCourant,dicomots.get(motCourant)+1);
        }
        else{
          if(motCourant != ""){
            dicomots.put(motCourant,1);
            }
          }
        System.out.println(dicomots);

        String motMax = null;
        Integer valMax = 0;
        Set<String> enscle = dicomots.keySet();
        for(String mot : enscle){
          if(motMax == null){
            motMax = mot;
            valMax = dicomots.get(mot);
          }
          else{
            if(valMax == dicomots.get(mot)){
              if(motMax.compareTo(mot) > 0){
                motMax = mot;
              }
            }
            else if(valMax < dicomots.get(mot)){
              motMax = mot;
              valMax = dicomots.get(mot);
            }
          }
        }

        return motMax;
    }

    public static boolean isParentheseGauche(String chaine){
      for(int i =0; i<chaine.length(); i++){
        if(chaine.charAt(i) == '('){
          return true;
        }
      }
      return false;
    }

    public static boolean isCrochetGauche(String chaine){
      for(int i =0; i<chaine.length(); i++){
        if(chaine.charAt(i) == '['){
          return true;
        }
      }
      return false;
    }


    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        boolean res = true;
        String chainePar = "";
        for(int i =0; i<chaine.length(); i++){
          if(chaine.charAt(i) == ')'){
            if(isParentheseGauche(chainePar)){
              Integer indice = chainePar.lastIndexOf('(');
              String mot = "";
              for(int j = 0; j<indice;j++ ){
                mot = mot + chainePar.charAt(j);
                }
              chainePar = mot;
            }
            else{
              return false;
            }
          }
          if(chaine.charAt(i) == '('){
            chainePar = chainePar + chaine.charAt(i);
          }
        }
        if(chainePar.length() != 0){
          res = false;
        }
      return res;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parenthèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
      boolean res = true;
      String chainePar = "";
      for(int i =0; i<chaine.length(); i++){
        if(chaine.charAt(i) == ')'){
          if(isParentheseGauche(chainePar)){
            Integer indice = chainePar.lastIndexOf('(');
            String mot = "";
            for(int j = 0; j<indice;j++ ){
              mot = mot + chainePar.charAt(j);
              }
            chainePar = mot;
          }
          else{
            return false;
          }
        }
        else if(chaine.charAt(i) == '(' || chaine.charAt(i) == '['){
          chainePar = chainePar + chaine.charAt(i);
        }
        else if(chaine.charAt(i) == ']'){
          if(isCrochetGauche(chainePar)){
            Integer indice = chainePar.lastIndexOf('[');
            String mot = "";
            for(int j = 0; j<indice;j++ ){
              mot = mot + chainePar.charAt(j);
              }
            chainePar = mot;
          }
          else{
            return false;
          }
        }
      }
      if(chainePar.length() != 0){
        res = false;
      }
    return res;
    }

    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
      boolean res = false;
      int iDebut = 0;
      int ifin = liste.size()-1;
      int indice = 0;
      while(!res && (iDebut < ifin)){
        if(liste.get(indice).compareTo(valeur) > 0){
          ifin = indice -1 ;
        }
        else if(liste.get(indice).compareTo(valeur) < 0){
          iDebut = indice + 1;
        }
        indice = (iDebut + ifin) / 2;
        if(liste.get(indice).compareTo(valeur) == 0){
          res = true;
        }
      }
      return res;
    }
}
